﻿using System.ComponentModel.DataAnnotations;

namespace BaiKiemTra.Models
{
    public class Reports
    {
        public int ReportsId { get; set; }
        [Required, StringLength(100)]
        public string AccountID { get; set; }
        [Range(0.01, 10000.00)]
        public string LogsID { get; set; }
        [Range(0.01, 10000.00)]
        public decimal TransactionalID { get; set; }
        [Range(0.01, 10000.00)]
        public string Reportname { get; set; }
        public string Reportdate { get; set; }
        public int AddTextHere { get; set; }
    }
}
