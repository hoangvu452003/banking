﻿using System.ComponentModel.DataAnnotations;

namespace BaiKiemTra.Models
{
    public class Transactions
    {
        public int TransactionsId { get; set; }
        [Required, StringLength(100)]
        public string EmployeeID { get; set; }
        [Range(0.01, 10000.00)]
        public string CustomerID { get; set; }
        [Range(0.01, 10000.00)]
        public string Name { get; set; }
        public int AddTextHere { get; set; }
    }
}
